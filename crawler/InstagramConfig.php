<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/30/16
 * Time: 4:47 PM
 */

namespace crawler;


class InstagramConfig
{
    const LOG_POST_COLLECTOR = "/log_post_coll.text";
    const LOG_POST_CONSUMER  = "/log_post_cons.text";
    const LOG_USER_COLLECTOR = "/log_user_coll.text";
    const LOG_USER_CONSUMER  = "/log_user_cons.text";

    const LOG_FOLDER        = "/log";
    const BACKUP_FOLDER     = "/backup";
    const POST_FOLDER       = "/Post";          // folder containing post from instagram
    const USER_QUEUE_FOLDER = "/UserQueue";     // folder containing poster per post entry
    const USER_FOLDER       = "/User";          // folder containing user info per post

    public static $TAGS                     = ["nofilter"];
    public static $INDEXES                  = ["instagram"];
    public static $ACCESS_TOKEN             = "1960304937.e029fea.fed49873d0d44f34b5751f37af4c349d";    // valid access token with public_access scope
    public static $STREAM_FORWARD           = false;    // put true to watch new data, false to track backward down to min Date threshold
    public static $INTERVAL                 = 5;        // base interval
    public static $COUNT                    = 5;       // number of data on collecting
    public static $MIN_DATE_THRESHOLD       = 0;        // min date cutoff when tracing backward
    public static $MIN_DATE_COUNT_THRESHOLD = 0.5;      // number of data below threshold to trigger the event (to avoid triggered by random old data when collecting)
    public static $UPPER_THRESHOLD          = 0.8;      // upper number of new data to trigger decreasing the interval
    public static $BOTTOM_THRESHOLD         = 0.2;      // bottom number of new data to trigger increasing the interval
    public static $MAX_INTERVAL             = 10;       // max interval to make sure it doesn't too long
    public static $MIN_INTERVAL             = 4;        // min interval to make sure it doesn't too fast

}