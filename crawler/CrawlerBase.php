<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/29/16
 * Time: 5:08 PM
 */

namespace crawler;

include_once __DIR__ . "/Thread.php";
include_once __DIR__ . "/CurlUtil.php";
include_once __DIR__ . "/FileUtil.php";
include_once __DIR__ . "/PdoUtil.php";

class CrawlerBase
{
    protected $log_file  = null;
    protected $parent_pid;
    private   $bufferLog = "";

    protected function logLine($message)
    {
        if ($this->log_file != null) {
            $this->createFolder(dirname($this->log_file));
            $file_open = fopen($this->log_file, 'a+');
            fwrite($file_open, date('Y-m-d H:i:s') . " : " . trim($message) . "\n");
            fclose($file_open);
        }
        @error_log($message, 0);
    }

    protected function tempLog($message)
    {
        $this->bufferLog .= $message;
    }

    protected function clearTempLog()
    {
        $this->bufferLog = "";
    }

    protected function flushLog()
    {
        if ($this->bufferLog != "") {
            $this->logLine($this->bufferLog);
            $this->bufferLog = "";
        }
    }

    protected function logDate()
    {
        if ($this->log_file != null) {
            $this->createFolder(dirname($this->log_file));
            $file_open = fopen($this->log_file, 'a+');
            fwrite($file_open, date('Y-m-d H:i:s') . " : ");
            fclose($file_open);
        }
    }

    protected function consoleLog($message)
    {
        @error_log($message, 0);
    }

    protected function createFolder($folder)
    {
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
            $this->logLine("Folder created: " . $folder);
        }
    }

    protected function rutime($ru, $rus, $index)
    {
        return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000))
            - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
    }

    protected function timeMillis()
    {
        $microtime = microtime();
        $comps     = explode(' ', $microtime);

        // Note: Using a string here to prevent loss of precision
        // in case of "overflow" (PHP converts it to a double)
        return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    }

    public function processExists($processName)
    {
        $exists = false;
        exec("ps -A | grep -i $processName | grep -v grep", $pids);
        if (count($pids) > 0) {
            $exists = true;
        }
        return $exists;
    }
}