<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/28/16
 * Time: 3:50 PM
 */

namespace crawler;


class PdoUtil
{

    public static $pdoPath = __DIR__;

    public static function initPath($path)
    {
        PdoUtil::$pdoPath = $path;
    }


    public static function dump_all()
    {
        $dir = PdoUtil::$pdoPath . '/instagram.db';
//        echo("sqlite path: $dir\n");
        $dbh = new \PDO("sqlite:$dir") or die("cannot open the database");
        $query  = "SELECT * FROM saved_state";
        $result = $dbh->query($query);
        if ($result != false)
            foreach ($result as $row) {
//            var_dump($row[0]);
                echo($row[0] . "-" . $row[1] . "\n");
            }
    }

    public static function getLastId($identifier)
    {
        $dir = PdoUtil::$pdoPath . '/instagram.db';
//        echo("sqlite path: $dir\n");
        $dbh = new \PDO("sqlite:$dir") or die("cannot open the database");
        $query  = "SELECT * FROM saved_state WHERE `job` = '$identifier'";
        $result = $dbh->query($query);
        if ($result != false)
            foreach ($result as $row) {
                return ($row[1]);
            }
        return null;
    }

    public static function setLastId($identifier, $lastId)
    {
        $dir = PdoUtil::$pdoPath . '/instagram.db';
//        echo("sqlite path: $dir\n");
        $dbh = new \PDO("sqlite:$dir") or die("cannot open the database");
        $query  = "INSERT OR REPLACE INTO saved_state (`job`,`last_id`) VALUES ('$identifier', '$lastId')";
        $result = $dbh->exec($query);
//        echo("set last id for $identifier - $lastId, result: $result\n");

    }
}