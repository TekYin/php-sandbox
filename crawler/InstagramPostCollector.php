<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/29/16
 * Time: 5:07 PM
 */
namespace crawler;

include_once __DIR__ . "/CrawlerBase.php";
include_once __DIR__ . "/InstagramConfig.php";

use crawler\CrawlerBase;
use crawler\CurlUtil;
use crawler\FileUtil;
use crawler\PdoUtil;
use crawler\Thread;

class InstagramPostCollector extends CrawlerBase
{
    private $folder_post;
    private $folder_user;
    private $interval;

    private $index;
    private $tag;
    private $streamForward;
    private $last_id;

    public function __construct()
    {
        InstagramConfig::$MIN_DATE_THRESHOLD = strtotime("-7 day");

        $this->log_file = __DIR__ . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_POST_COLLECTOR;

        $this->folder_post = __DIR__ . InstagramConfig::POST_FOLDER;
        $this->folder_user = __DIR__ . InstagramConfig::USER_QUEUE_FOLDER;
        $this->interval    = 5;
        PdoUtil::$pdoPath  = __DIR__;
        date_default_timezone_set("Asia/Jakarta");
    }

    public function launchManager()
    {
        $this->parent_pid = getmypid();

        $TAGS    = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $t = new Thread([$this, "createThread"]);
            $t->start($INDEXES[$i], $TAGS[$i], true);
            $t->start($INDEXES[$i], $TAGS[$i], false);
        }

        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    public function ascending($a, $b)
    {
        return strcmp($a['id'], $b['id']);
    }

    public function descending($a, $b)
    {
        return strcmp($b['id'], $a['id']);
    }

    private function generateInstagramTagUrl($tag, $token, $min_id = null, $max_id = null)
    {
        $BASE_URL = "https://api.instagram.com/v1/tags/%s/media/recent?access_token=%s&count=%d";
        $url      = sprintf($BASE_URL, $tag, $token, 30);
        if ($min_id != null) {
            $url .= "&min_tag_id=" . $min_id;
        } else if ($max_id != null) {
            $url .= "&max_tag_id=" . $max_id;
        }
        return $url;
    }

    public function demo()
    {
        $max = null;
        while (1) {
            $url        = $this->generateInstagramTagUrl("nofilter", InstagramConfig::$ACCESS_TOKEN, $max, null);
            $result     = CurlUtil::GET($url);
            $pagination = $result["pagination"];
            $data_arr   = $result["data"];
            usort($data_arr, [$this, "descending"]);

            for ($i = 0; $i < count($data_arr); $i++) {
                $data = $data_arr[$i];
                echo $data["created_time"] . "\t" . "id-" . $data["id"] . "\n";
            }
            $max = $pagination["next_max_tag_id"];
        }
    }

    public function createThread($index, $tag, $streamForward)
    {
        // everything in here is persistent to each Thread
        $this->index         = $index;
        $this->tag           = $tag;
        $this->streamForward = $streamForward;
        $this->logLine("Starting thread, index: $index, tag: $tag");

        $min = $streamForward ? null : PdoUtil::getLastId($this->index . "-" . $this->tag);
        $max = $streamForward ? PdoUtil::getLastId($this->index . "-" . $this->tag) : null;
//        $this->logLine("min: " . $min . ", max: " . $max);
        while (true) {
            $lastCheck = time();

            $url = $this->generateInstagramTagUrl($this->tag, InstagramConfig::$ACCESS_TOKEN, $min, $max);
            try {
                $time_start = microtime(true);
                $tag        = $this->fetchData($url);
                if ($this->streamForward) {
                    $min = $tag;
                } else {
                    $max = $tag;
                }
                $time_end       = microtime(true);
                $execution_time = ($time_end - $time_start);
                $this->tempLog(", fetch time: " . $execution_time);
                $this->flushLog();
            } catch (\Exception $e) {
                $this->logLine($e->getMessage());
                $this->clearTempLog();
            }
            if (!$this->processExists($this->parent_pid)) {
                echo("parent die $this->tag\n");
                break;
            }
            if ($this->streamForward)
                // Wait until ready for next check
                while (time() - $lastCheck < $this->interval) {
                    sleep(1);
                }
            else
                sleep(1);
        }
    }

    private function fetchData($url)
    {
        $response   = CurlUtil::GET($url);
        $data_arr   = $response["data"];
        $pagination = $response["pagination"];

        $this->tempLog("got content for $this->tag (" . ($this->streamForward ? "Forw" : "Back") . "), item: " . count($data_arr) . ", ");
        if ($this->streamForward) {
            $this->trackForward($data_arr);
        } else {
            $this->trackBackward($data_arr);
        }

        PdoUtil::setLastId($this->index . "-" . $this->tag, $pagination["next_max_tag_id"]);
        return $pagination["next_max_tag_id"];
    }

    private function trackForward($data_arr)
    {
        usort($data_arr, [$this, "ascending"]);

        // index for last_id from last collect
        $newDataIndex = -1;
        for ($i = 0; $i < count($data_arr); $i++) {
            $data = $data_arr[$i];
            if (strcmp($data['id'], $this->last_id) == 0) {
                $newDataIndex = $i;
            }
        }

        // getting new latest id
        $lastData = $data_arr[count($data_arr) - 1];
        $this->tempLog("date: " . $lastData["created_time"]);
        $this->last_id = $lastData['id'];

        // removing old data based on last_id
        if ($newDataIndex >= 0)
            $new_arr = array_splice($data_arr, $newDataIndex);
        else {
            $new_arr = $data_arr;
        }

        $this->tempLog(", new item: " . count($new_arr));


        // monitor new data count to adjust interval
        $data_count = count($new_arr);
        $max_count  = InstagramConfig::$COUNT;
        if ($data_count > $max_count * InstagramConfig::$UPPER_THRESHOLD) { // if data is more than threshold, we need to decrease the interval to make sure collector can catchup all new data
            $this->interval = max($this->interval - 0.5, InstagramConfig::$MIN_INTERVAL);
        } else if ($data_count < $max_count * InstagramConfig::$BOTTOM_THRESHOLD) { // if data is less than threshold, wee need to increase the interval to save the memory and bandwidth
            $this->interval = min($this->interval + 0.5, InstagramConfig::$MAX_INTERVAL);
        }

        $this->saveUserToFile($new_arr);
        FileUtil::writeToFile($this->folder_post . "/" . $this->index . "-" . $this->tag . '/Instagram.' . date('Ymd-His-') . rand(1000, 9000) . '.queue', json_encode($new_arr));
    }

    private function trackBackward($data_arr)
    {
        usort($data_arr, [$this, "descending"]);

        $threshold = 0;
        for ($i = 0; $i < count($data_arr); $i++) {
            $data = $data_arr[$i];
            if ($data['created_time'] < InstagramConfig::$MIN_DATE_THRESHOLD) {
                echo("old data below threshold found.\n");
                $threshold++;
            }
        }
        $lastData = $data_arr[count($data_arr) - 1];
        $this->tempLog("date: " . $lastData["created_time"]);


        if ($threshold >= InstagramConfig::$COUNT * InstagramConfig::$MIN_DATE_COUNT_THRESHOLD) {
            echo("data below min date is exceeding threshold, stop the collection?\n");
            //TODO handle the stop mechanism
        }

        $this->saveUserToFile($data_arr);
        FileUtil::writeToFile($this->folder_post . "/" . $this->index . "-" . $this->tag . '/Instagram.' . date('Ymd-His-') . rand(1000, 9000) . '.queue', json_encode($data_arr));
    }

    public function saveUserToFile($data_arr)
    {
        $users = [];
        if (isset($data_arr)) {
            foreach ($data_arr as $dt) {
                $users[] = $dt["id"] . "-" . $dt["user"]["id"];
            }
            FileUtil::writeToFile($this->folder_user . "/" . $this->index . "-" . $this->tag . '/Instagram.UserQueue.' . date('Ymd-His-') . rand(1000, 9000) . '.queue', json_encode($users));
        }
    }
}

$collector = new InstagramPostCollector();
$collector->launchManager();