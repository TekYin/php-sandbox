<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 12/21/16
 * Time: 4:55 PM
 */

namespace crawler;

include_once __DIR__ . "/CrawlerBase.php";
include_once __DIR__ . "/InstagramConfig.php";
include_once __DIR__ . "/InstagramParser.php";

use crawler\CrawlerBase;
use crawler\CurlUtil;
use crawler\FileUtil;
use crawler\Thread;

class InstagramUserConsumer extends CrawlerBase
{
    private $folder_user;
    private $interval;
    private $dir;
    private $index;
    private $tag;

    public function __construct()
    {
        $this->log_file = __DIR__ . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_USER_CONSUMER;

        $this->folder_user = __DIR__ . InstagramConfig::USER_FOLDER;
        $this->interval    = 5;

        date_default_timezone_set("Asia/Jakarta");
    }

    public function launchManager()
    {
        $this->parent_pid = getmypid();

        $TAGS    = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $dir = $this->folder_user . "/" . $INDEXES[$i] . "-" . $TAGS[$i];
            $t   = new Thread([$this, "createThread"]);
            $t->start($dir, true);
        }
        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    public function createThread($dir)
    {
        $this->dir   = $dir;
        $part        = pathinfo($dir);
        $full_part   = explode("-", $part["basename"]);
        $this->index = $full_part[0];
        $this->tag   = $full_part[1];

        $this->logLine("Post folder to process : $dir");
        while (true) {
            // Get a list of queue files
            $queueFiles = glob($this->dir . '/Instagram*.queue');
            $lastCheck  = time();

            if (count($queueFiles) > 0) {
                $this->logLine('Found ' . count($queueFiles) . ' queue files to process...');
                // Iterate over each file (if any)
                foreach ($queueFiles as $queueFile) {
                    $this->processQueueFile($queueFile);
                }
            } else {
                $this->logLine("no file found on " . $this->dir . ", sleeping");
            }

            if (!$this->processExists($this->parent_pid)) {
                echo("parent die $this->tag\n");
                break;
            }

            // Wait until ready for next check
            while (time() - $lastCheck < $this->interval) {
                sleep(1);
            }
        }

    }

    private function processQueueFile($queueFile)
    {
        $string_content = file_get_contents($queueFile);
        $data           = json_decode($string_content, true);

        $time_start = microtime(true);
        if (isset($data)) {
            $part  = explode(".", $queueFile);
            $posId = $part[2];

            $this->logLine('Processing user file: ' . $queueFile . " for post: " . $posId);

            $mapped = InstagramParser::parseInstagramUser($data);
            $url    = InstagramParser::getElasticSearchUrlForUser($this->index, $posId);
            $this->logLine(json_encode($mapped));
            $this->logLine($url);
            while (true) { // loop until the CURL is success
                try {
//                    usleep(100000);
                    $response = CurlUtil::POST($url, $mapped);
                    $this->logLine("Curl ok: ".json_encode($response));
                    break;
                } catch (\Exception $e) {
                    $this->logLine($e->getMessage());
                    $this->clearTempLog();
                }
                sleep(1);
            }

        }
        $time_end       = microtime(true);
        $execution_time = ($time_end - $time_start);
        $this->logLine("process time: " . $execution_time);

        // move file to backup folder
        $this->createFolder(dirname(dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($queueFile, "."))));
        rename($queueFile, dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($queueFile, ".")));
//        unlink($queueFile);
        $this->flushLog();
    }

}


$collector = new InstagramUserConsumer();
$collector->launchManager();