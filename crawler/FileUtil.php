<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/29/16
 * Time: 4:56 PM
 */

namespace crawler;


class FileUtil
{
    public static function appendToFile($path, $message)
    {
        $file = fopen($path, 'a+');
        fwrite($file, $message);
        fclose($file);
    }

    public static function writeToFile($path, $message)
    {
        if (!file_exists(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }

        $file = fopen($path, 'w+');
        fwrite($file, $message);
        fclose($file);
    }

    public static function readFile($path)
    {
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            return $contents;
        } else return false;
    }

    /**
     * Return a relative path to a file or directory using base directory.
     * When you set $base to /website and $path to /website/store/library.php
     * this function will return /store/library.php
     *
     * Remember: All paths have to start from "/" or "\" this is not Windows compatible.
     *
     * @param   String $base A base path used to construct relative path. For example /website
     * @param   String $path A full path to file or directory used to construct relative path. For example /website/store/library.php
     *
     * @return  String
     */
    public static function getRelativePath($base, $path)
    {
        // Detect directory separator
        $separator = substr($base, 0, 1);
        $base      = array_slice(explode($separator, rtrim($base, $separator)), 1);
        $path      = array_slice(explode($separator, rtrim($path, $separator)), 1);

        return $separator . implode($separator, array_slice($path, count($base)));
    }
}