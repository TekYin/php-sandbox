<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/29/16
 * Time: 4:56 PM
 */

namespace crawler;


class CurlUtil
{
    public static function GET($request_url, $json_data = "")
    {
        $data_string = json_encode($json_data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($request_url, ' '));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds

        if (!empty($json_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                ]
            );
        }

        $output = curl_exec($ch);
        $error  = curl_error($ch);
        curl_close($ch);

        if ($output == '') {
            throw new \Exception("CURL Error: " . $error);
        } else {
            return json_decode($output, true);
        }
    }

    public static function simpleGET($request_url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($request_url, ' '));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        $output = curl_exec($ch);
        curl_close($ch);
        if ($output === false) {
            return curl_error($ch);
        } else {
            return $output;
        }
    }

    public static function POST($url, $json_data = "")
    {
        $data_string = json_encode($json_data);

        $ch = curl_init(trim($url, ' '));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        if (!empty($json_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                ]
            );
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        }

        $output = curl_exec($ch);
        $error  = curl_error($ch);
        curl_close($ch);

        if ($output == '') {
            throw new \Exception("CURL Error: " . $error);
        } else {
            return json_decode($output, true);
        }
    }

    public static function PUT($url, $json_data = "")
    {
        $data_string = json_encode($json_data);

        $ch = curl_init(trim($url, ' '));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
//        curl_setopt($ch, CURLOPT_PROXY, "127.0.0.1:8888");
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds

        if (!empty($json_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                ]
            );
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        }

        $output = curl_exec($ch);
        $error  = curl_error($ch);
        curl_close($ch);
        echo("'".$output."'");
        if ($output == '' || $output == false) {
            throw new \Exception("CURL Error: " . $error);
        } else {
            return json_decode($output, true);
        }
    }
}