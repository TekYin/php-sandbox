<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 12/21/16
 * Time: 4:26 PM
 */

namespace crawler;

include_once __DIR__ . "/CrawlerBase.php";
include_once __DIR__ . "/InstagramConfig.php";

use crawler\CrawlerBase;
use crawler\CurlUtil;
use crawler\FileUtil;
use crawler\PdoUtil;
use crawler\Thread;

class InstagramUserCollector extends CrawlerBase
{
    private $folder_user_queue;
    private $folder_user;
    private $interval;
    private $index;
    private $tag;

    public function __construct()
    {
        $this->log_file = __DIR__ . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_USER_COLLECTOR;

        $this->folder_user_queue = __DIR__ . InstagramConfig::USER_QUEUE_FOLDER;
        $this->folder_user       = __DIR__ . InstagramConfig::USER_FOLDER;
        $this->interval          = 5;

        date_default_timezone_set("Asia/Jakarta");
    }

    public function launchManager()
    {
        $this->parent_pid = getmypid();

        $TAGS    = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $t = new Thread([$this, "createThread"]);
            $t->start($INDEXES[$i], $TAGS[$i]);
        }

        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    public function createThread($index, $tag)
    {
        $this->index = $index;
        $this->tag   = $tag;
        $this->logLine("Starting thread, index: $index, tag: $tag");

        $start = microtime(true);

        while (true) {
            $current_queue = glob($this->folder_user_queue . "/$index-$tag/Instagram*.queue");
            $this->logLine('Found ' . count($current_queue) . ' queue files to process...');

            foreach ($current_queue as $file) {
                while (true) { // loop until the CURL is success
                    if (!$this->processExists($this->parent_pid)) {
                        echo("parent die $this->tag\n");
                        return;
                    }
                    try {
                        $this->process_file($file);
                        $this->logLine("file process ok");
                        break;
                    } catch (\Exception $e) {
                        $this->logLine($e->getMessage());
                        $this->clearTempLog();
                    }
                    sleep(1);
                }
            }
            sleep($this->interval);
        }
        $time_elapsed_secs = microtime(true) - $start;
        $this->logLine("time elapsed: " . $time_elapsed_secs);
        
    }

    private function process_file($file)
    {
        $content = file_get_contents($file);
        $users   = json_decode($content, true);
        $this->logLine('Found ' . count($users) . ' user to process...');

        foreach ($users as $user) { // loop all the user to get the User information
            $part     = explode("-", $user);
            $user_id  = $part[1];
            $url      = $this->generateInstagramUserUrl($user_id, InstagramConfig::$ACCESS_TOKEN);
            $userData = CURLUtil::GET($url)["data"];
            FileUtil::writeToFile($this->folder_user . "/" . $this->index . "-" . $this->tag . "/Instagram.UserPost." . $part[0] . ".queue", json_encode($userData));
        }
        $this->logLine("folder: " . dirname(dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($file, "."))));
        $this->createFolder(dirname(dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($file, "."))));
        rename($file, dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($file, ".")));
    }


    public static function generateInstagramUserUrl($user_id, $token)
    {
        $BASE_URL = "https://api.instagram.com/v1/users/%s/?access_token=%s";
        $url      = sprintf($BASE_URL, $user_id, $token);
        return $url;
    }
}

$client
    = new InstagramUserCollector();
$client->launchManager();