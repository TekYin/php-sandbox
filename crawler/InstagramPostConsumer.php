<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 12/6/16
 * Time: 10:46 AM
 */

namespace crawler;

include_once __DIR__ . "/CrawlerBase.php";
include_once __DIR__ . "/InstagramConfig.php";
include_once __DIR__ . "/InstagramParser.php";

use crawler\CrawlerBase;
use crawler\CurlUtil;
use crawler\FileUtil;
use crawler\Thread;

class InstagramPostConsumer extends CrawlerBase
{
    private $folder_post;
    private $interval;

    private $dir;
    private $index;
    private $tag;

    public function __construct()
    {
        $this->log_file = __DIR__ . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_POST_CONSUMER;

        $this->folder_post = __DIR__ . InstagramConfig::POST_FOLDER;
        $this->interval    = 5;

        date_default_timezone_set("Asia/Jakarta");
    }


    public function launchManager()
    {
        $this->parent_pid = getmypid();

        $TAGS    = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $dir = dirname(__FILE__) . InstagramConfig::POST_FOLDER . "/" . $INDEXES[$i] . "-" . $TAGS[$i];
            $t   = new Thread([$this, "createThread"]);
            $t->start($dir, true);
        }
        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    public function createThread($dir)
    {
        $this->dir   = $dir;
        $part        = pathinfo($dir);
        $full_part   = explode("-", $part["basename"]);
        $this->index = $full_part[0];
        $this->tag   = $full_part[1];

        $this->logLine("Post folder to process : $dir");
        while (true) {
            // Get a list of queue files
            $queueFiles = glob($this->dir . '/*.queue');
            $lastCheck  = time();

            if (count($queueFiles) > 0) {
                $this->logLine('Found ' . count($queueFiles) . ' queue files to process...');
                // Iterate over each file (if any)
                foreach ($queueFiles as $queueFile) {
                    $this->processQueueFile($queueFile);
                }
            } else {
                $this->logLine("no file fount on " . $this->dir . ", sleeping");
            }

            if (!$this->processExists($this->parent_pid)) {
                echo("parent die $this->tag\n");
                break;
            }

            // Wait until ready for next check
            while (time() - $lastCheck < $this->interval) {
                sleep(1);
            }
        }

    }

    private function processQueueFile($queueFile)
    {
        $string_content = file_get_contents($queueFile);
        $data_content   = json_decode($string_content, true);

        $time_start = microtime(true);
        if (isset($data_content)) {
            $this->logLine('Processing file: ' . $queueFile . " , data count: " . count($data_content));

            foreach ($data_content as $data) {
                $mapped = InstagramParser::parseInstagramPost($data);
                $url    = InstagramParser::getElasticSearchUrl($this->index, $mapped["id"]);

                while (true) { // loop until the CURL is success
                    try {
//                        usleep(100000);
//                        $log = CurlUtil::PUT($url, $mapped);
                        $response = CurlUtil::PUT($url, $mapped);
                        $this->logLine("Curl ok: ". json_encode($response));
//                        $this->logLine("Curl ok");
                        break;
                    } catch (\Exception $e) {
                        $this->logLine($e->getMessage());
                        $this->clearTempLog();
                    }
                    sleep(1);
                }
            }

        }
        $time_end       = microtime(true);
        $execution_time = ($time_end - $time_start);
        $this->logLine("process time: " . $execution_time);

        // move file to backup folder
        $this->createFolder(dirname(dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($queueFile, "."))));
        rename($queueFile, dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileUtil::getRelativePath(dirname(__FILE__), ltrim($queueFile, ".")));
//        unlink($queueFile);
        $this->flushLog();
    }

}


$collector = new InstagramPostConsumer();
$collector->launchManager();