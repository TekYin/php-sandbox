<?php

namespace crawler;
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/21/16
 * Time: 5:26 PM
 */
class InstagramParser
{
    public static function parseInstagramPost($json)
    {
        //tags
        $tags = [];
        $a    = 0;
        if (!empty($json['tags'])) {
            foreach ($json['tags'] as $tag) {
                $tags[$a]['text'] = $tag;
                $a++;
            }
        }

        //created_time
        $created_time = [];
        if (!empty($json['created_time'])) {
            $created_time['unix_timestamp'] = $json['created_time'];
            $created_time['datetime']       = date('Y-m-d H:i:s', $json['created_time']);
        }

        //caption
        $caption = [];
        if (!empty($json['caption'])) {
            $caption['created_time']['unix_timestamp'] = $json['caption']['created_time'];
            $caption['created_time']['datetime']       = date('Y-m-d H:i:s', $json['caption']['created_time']);
            $caption['text']['raw']                    = $json['caption']['text'];
            $caption['text']['text']                   = $json['caption']['text'];
            $caption['text']['std']                    = $json['caption']['text'];
            $caption['from']                           = $json['caption']['from'];
            $caption['id']                             = $json['caption']['id'];
        }

        $instagram_results = [
            "attribution"    => $json['attribution'],
            "tags"           => $tags,
            "type"           => $json['type'],
            "location"       => $json['location'],
            "comments"       => $json['comments'],
            "filter"         => $json['filter'],
            "created_time"   => $created_time,
            "link"           => $json['link'],
            "likes"          => $json['likes'],
            "images"         => $json['images'],
            "users_in_photo" => $json['users_in_photo'],
            "caption"        => $caption,
            "user_has_liked" => $json['user_has_liked'],
            "id"             => $json['id'],
            "user"           => $json['user']
        ];
        return $instagram_results;
    }

    public static function getElasticSearchUrl($index, $postId)
    {
        return "http://lisento_adm:l1sento_4dm@128.199.240.215:9200/$index/instagram/$postId/_create";
    }

    public static function parseInstagramUser($data)
    {
        $instagram_user_results = [
            "doc" => [
                "user" => [
                    "bio" => $data['bio'],
                    "website"  => $data['website'],
                    "counts" => $data['counts']
                ]
            ]
        ];
        return $instagram_user_results;
    }

    public static function getElasticSearchUrlForUser($index, $postId)
    {
        return "http://lisento_adm:l1sento_4dm@128.199.240.215:9200/$index/instagram/$postId/_update";
    }
}