<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/16/16
 * Time: 10:37 AM
 */

include_once "InstagramConfig.php";
include_once "ServiceBase.php";
include_once "CURLService.php";
include_once "FileService.php";

class InstagramUserConsumer extends ServiceBase
{
    private $checkInterval;

    public function __construct()
    {
        $this->log_file = InstagramConfig::LOG_USER_CONSUMER;

        if (!file_exists('./log/')) {
            mkdir('./log/', 0777, true);
        }
        if (!file_exists(InstagramConfig::BACKUP_FOLDER)) {
            mkdir(InstagramConfig::BACKUP_FOLDER, 0777, true);
        }
        if (!file_exists(InstagramConfig::BACKUP_FOLDER . ltrim(InstagramConfig::USER_FOLDER, "."))) {
            mkdir(InstagramConfig::BACKUP_FOLDER . ltrim(InstagramConfig::USER_FOLDER, "."), 0777, true);
        }

        $this->checkInterval = InstagramConfig::$INTERVAL;
    }

    public function process()
    {
        $lastCheck = 0;

        while (true) {
            // Get a list of queue files
            $queueFiles = glob(InstagramConfig::USER_FOLDER . '/*.queue');
            $lastCheck = time();

            if (count($queueFiles) > 0) {
                $this->log('Found ' . count($queueFiles) . ' queue files to process...');
                // Iterate over each file (if any)
                foreach ($queueFiles as $queueFile) {
                    $this->processQueueFile($queueFile);
                }
            }
            // Wait until ready for next check
            while (time() - $lastCheck < $this->checkInterval) {
                sleep(1);
            }
        }
    }

    private function processQueueFile($queueFile)
    {
        $string_content = file_get_contents($queueFile);
        $data_content = json_decode($string_content, true);

        $this->log('Processing file: ' . $queueFile . " , data count: " . count($data_content));
        if (isset($data_content)) {
            foreach ($data_content as $data) {

                //TODO activate this after parsePost is finished
//                $instagram_mapping = $this->parsePost($data);
                //TODO check lisento url
//                $url = 'http://lisento_adm:l1sento_4dm@128.199.240.215:9200' . '/index/' . 'instagram' . '/' . $data['id'] . '/_create';
//                $log = CURLService::PUTRequest($url, $instagram_mapping);
            }
            sleep(1);
        }

        // move file to backup folder
        rename($queueFile, InstagramConfig::BACKUP_FOLDER . ltrim($queueFile, "."));
    }

    private function parsePost($data)
    {
        // TODO write parser to elastic here
        return true;
    }

}

$a = new InstagramPostConsumer();
$a->process();