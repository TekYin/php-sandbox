<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/15/16
 * Time: 11:54 AM
 */
include_once "InstagramConfig.php";

class ServiceBase

{
    public $log_file = null;
    protected $parent_pid;

    public function log($message)
    {
        if ($this->log_file != null) {
            $file_open = fopen($this->log_file, 'a+');
            fwrite($file_open, date('Y-m-d H:i:s') . " : " . trim($message) . "\n");
            fclose($file_open);
        }

        @error_log($message, 0);
    }

    public function createFolder($folder)
    {
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
            $this->log("creating folder: " . $folder);
        }
    }

    public function processExists($processName)
    {
        $exists = false;
        exec("ps -A | grep -i $processName | grep -v grep", $pids);
        if (count($pids) > 0) {
            $exists = true;
        }
        return $exists;
    }
}