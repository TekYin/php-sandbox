<?php

class CURLService
{
    public static function GETRequest($request_url, $json_data = "")
    {
        $data_string = json_encode($json_data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($request_url, ' '));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        if (!empty($json_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                )
            );
        }

        $output = curl_exec($ch);
        if ($output === false) {
            $response = [
                'status' => 'curl error',
                'data' => curl_error($ch)
            ];
        } else {
            $response = [
                'status' => 'curl operation completed',
                'data' => json_decode($output, true)
            ];
        }
        curl_close($ch); 
        return $response;
    }

    public static function simpleGET($request_url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($request_url, ' '));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        $output = curl_exec($ch);
        curl_close($ch);
        if ($output === false) {
            return curl_error($ch);
        } else {
            return $output;
        }
    }

    public static function PUTRequest($url, $json_data = "")
    {
        $data_string = json_encode($json_data);

        $ch = curl_init(trim($url, ' '));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

        if (!empty($json_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                )
            );
        } else {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }

        $output = curl_exec($ch);
        if ($output === false) {
            $response = [
                'status' => 'curl error',
                'data' => curl_error($ch)
            ];
        } else {
            $response = [
                'status' => 'curl operation completed',
                'data' => json_decode($output)
            ];
        }
        curl_close($ch);
        return $response;
    }
}