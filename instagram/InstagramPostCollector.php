<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/10/16
 * Time: 3:24 PM
 */
include_once "InstagramConfig.php";
include_once "CURLService.php";
include_once "FileService.php";
include_once "ServiceBase.php";
include_once "Thread.php";
include_once "PdoService.php";

class InstagramCollector extends ServiceBase
{
    private $interval = 5;
    private $last_id = "";
    private $tag;
    private $index;
    private $post_folder;
    private $user_queue_folder;

    public function InstagramCollector()
    {
        //         DEBUG
        InstagramConfig::$MIN_DATE_THRESHOLD = strtotime("-7 day");
        // END DEBUG

        $this->log_file = dirname(__FILE__) . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_POST_COLLECTOR;

        $this->post_folder = dirname(__FILE__) . InstagramConfig::POST_FOLDER;
        $this->user_queue_folder = dirname(__FILE__) . InstagramConfig::USER_QUEUE_FOLDER;

        $this->createFolder(dirname(__FILE__) . InstagramConfig::LOG_FOLDER);
        $this->createFolder($this->post_folder);
        $this->createFolder($this->user_queue_folder);

        date_default_timezone_set("Asia/Jakarta");


        $this->interval = InstagramConfig::$INTERVAL;
    }

    public function start()
    {
        // read TAGS
        $this->parent_pid = getmypid();
        $TAGS = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $this->executeThread($TAGS[$i], $INDEXES[$i]);
//        }
//        
//        foreach ($TAGS as $tag) {
        }

        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    private function executeThread($tag, $index)
    {
        $t = new Thread([$this, "startThread"]);
        $t->start($tag, $index);
    }

    public function startThread($tag, $index)
    {
        // What happened in here is only saved in thread, main Class doesn't affected
        $this->tag = $tag;
        $this->index = $index;

        $this->log("tag: " . $tag);


        $max = PdoService::getLastId($this->index . "-" . $this->tag);
        $min = null;
        if ($max != null)
            echo $this->index . "-" . $this->tag . " have saved state, load last id: " . $max . "\n";

        while (true) {
            $url = $this->generateInstagramTagUrl($this->tag, InstagramConfig::$ACCESS_TOKEN, $min, $max);
            $tag = $this->fetchInstagramContent($url);
            if (InstagramConfig::$STREAM_FORWARD) {
                $min = $tag;
            } else {
                $max = $tag;
            }
            echo("sleep: " . $this->interval . "\n");

            if (!$this->processExists($this->parent_pid)) {
                echo("parent die $this->tag\n");
                break;
            }

            sleep($this->interval);
        }
    }

    public function generateInstagramTagUrl($tag, $token, $min_id = null, $max_id = null)
    {
        $BASE_URL = "https://api.instagram.com/v1/tags/%s/media/recent?access_token=%s&count=%d";
        $url = sprintf($BASE_URL, $tag, $token, InstagramConfig::$COUNT);
        if ($min_id != null) {
            $url .= "&min_tag_id=" . $min_id;
        } else if ($max_id != null) {
            $url .= "&max_tag_id=" . $max_id;
        }
        return $url;
    }

    public function ascending($a, $b)
    {
        return strcmp($a['id'], $b['id']);
    }

    public function descending($a, $b)
    {
        return strcmp($b['id'], $a['id']);
    }

    public function fetchInstagramContent($url)
    {
        $curl_response = CURLService::GETRequest($url);
        $json = $curl_response['data'];

        $next_max = $json['pagination']['next_max_tag_id'];
        $next_min = $json['pagination']['next_max_tag_id'];

        $data_arr = $json['data'];
        $this->log("got content for $this->tag, item: " . count($data_arr));

        if (!InstagramConfig::$STREAM_FORWARD) { // going backward, need to monitor date cutoff
            //TODO make sure data collected is optimal and fetch right data
            usort($data_arr, array($this, "descending"));

            $threshold = 0;
            for ($i = 0; $i < count($data_arr); $i++) {
                $data = $data_arr[$i];
//                echo("date: " . date('Y-m-d H:i:s', $data['created_time']) . " - " . $data['id'] . "\n");
                if ($data['created_time'] < InstagramConfig::$MIN_DATE_THRESHOLD) {
                    echo("old data below threshold found.\n");
                    $threshold++;
                }
            }
            if ($threshold >= InstagramConfig::$COUNT * InstagramConfig::$MIN_DATE_COUNT_THRESHOLD) {
                echo("data below min date is exceeding threshold, stop the collection?\n");
                //TODO handle the stop mechanism
            }
            // save last id to sqlite
            if (count($data_arr) > 0) {
                PdoService::setLastId($this->index . "-" . $this->tag, $next_max);
            }

            $this->saveUserToFile($data_arr);

            $this->createFolder($this->post_folder . "/" . $this->index . "-" . $this->tag);
            FileService::Write_to_file($this->post_folder . "/" . $this->index . "-" . $this->tag . '/Instagram.' . date('Ymd-His') . '.queue', json_encode($data_arr));
            return $next_max;
        } else { // going forward need special care to avoid processing redundant data
            usort($data_arr, array($this, "ascending"));
            // index for last_id from last collect
            $newDataIndex = -1;
            for ($i = 0; $i < count($data_arr); $i++) {
                $data = $data_arr[$i];
                if (strcmp($data['id'], $this->last_id) == 0) {
                    $newDataIndex = $i;
                }
            }
            // getting new latest id
            $this->last_id = $data_arr[count($data_arr) - 1]['id'];

            // removing old data based on last_id
            if ($newDataIndex >= 0)
                $new_arr = array_splice($data_arr, $newDataIndex);
            else {
                $new_arr = $data_arr;
            }

            // monitor new data count to adjust interval
            $data_count = count($new_arr);
            $max_count = InstagramConfig::$COUNT;
            if ($data_count > $max_count * InstagramConfig::$UPPER_THRESHOLD) { // if data is more than threshold, we need to decrease the interval to make sure collector can catchup all new data
                $this->interval = max($this->interval - 0.5, InstagramConfig::$MIN_INTERVAL);
            } else if ($data_count < $max_count * InstagramConfig::$BOTTOM_THRESHOLD) { // if data is less than threshold, wee need to increase the interval to save the memory and bandwidth
                $this->interval = min($this->interval + 0.5, InstagramConfig::$MAX_INTERVAL);
            }

            $this->saveUserToFile($new_arr);

            $this->createFolder($this->post_folder . "/" . $this->index . "-" . $this->tag);
            FileService::Write_to_file($this->post_folder . "/" . $this->index . "-" . $this->tag . '/Instagram.' . date('Ymd-His') . '.queue', json_encode($new_arr));
            return $next_max;
        }
    }

    /**
     * @param $data_arr
     */
    public function saveUserToFile($data_arr)
    {
        $users = [];
        if (isset($data_arr)) {

            foreach ($data_arr as $dt) {
                $users[] = $dt["id"] . "-" . $dt["user"]["id"];
            }
            $this->createFolder($this->user_queue_folder . "/" . $this->index . "-" . $this->tag);
            FileService::Write_to_file($this->user_queue_folder . "/" . $this->index . "-" . $this->tag . '/Instagram.UserQueue.' . date('Ymd-His') . '.queue', json_encode($users));
        }
    }

}

$i = new InstagramCollector();
$i->start();
//echo "null: " . null;
//echo("get aaa: " . PdoService::getLastId("aaa") . "\n");
//echo("get sdf: " . PdoService::getLastId("sdf") . "\n");
//PdoService::setLastId("sdf", "hahaha");
//echo("get sdf: " . PdoService::getLastId("sdf") . "\n");
//PdoService::setLastId("123a", "asdashahaha");
//PdoService::dump_all();