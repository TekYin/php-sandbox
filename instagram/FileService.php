<?php

class FileService
{
    public static function Append_to_file($path, $message)
    {
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            $results = $contents . "\n" . $message;
        } else $results = $message;

        $file = fopen($path, 'w+');
        fwrite($file, $results);
        fclose($file);
    }

    public static function Write_to_file($path, $message)
    {
        $part = pathinfo($path);
        if (!file_exists($part["dirname"])) {
            
            mkdir($part["dirname"], 0777, true);
        }

        $file = fopen($path, 'w+');
        fwrite($file, $message);
        fclose($file);
    }

    public static function Read_file($path)
    {
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            return $contents;
        } else return false;
    }

    /**
     * Return a relative path to a file or directory using base directory.
     * When you set $base to /website and $path to /website/store/library.php
     * this function will return /store/library.php
     *
     * Remember: All paths have to start from "/" or "\" this is not Windows compatible.
     *
     * @param   String $base A base path used to construct relative path. For example /website
     * @param   String $path A full path to file or directory used to construct relative path. For example /website/store/library.php
     *
     * @return  String
     */
    public static function getRelativePath($base, $path)
    {
        // Detect directory separator
        $separator = substr($base, 0, 1);
        $base = array_slice(explode($separator, rtrim($base, $separator)), 1);
        $path = array_slice(explode($separator, rtrim($path, $separator)), 1);

        return $separator . implode($separator, array_slice($path, count($base)));
    }
}