<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/16/16
 * Time: 10:37 AM
 */

include_once "InstagramConfig.php";
include_once "InstagramParser.php";
include_once "ServiceBase.php";
include_once "CURLService.php";
include_once "FileService.php";
include_once "Thread.php";

class InstagramPostConsumer extends ServiceBase
{

    private $checkInterval;
    private $dir;
    private $tag;
    private $path;
    private $index;
    private $backup_folder;

    public function __construct()
    {
        $this->log_file = dirname(__FILE__) . InstagramConfig::LOG_FOLDER . InstagramConfig::LOG_POST_CONSUMER;
        $this->backup_folder = dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . ltrim(InstagramConfig::POST_FOLDER, ".");

        $this->createFolder(dirname(__FILE__) . InstagramConfig::LOG_FOLDER);
        $this->createFolder($this->backup_folder);

        $this->checkInterval = InstagramConfig::$INTERVAL;
    }

    public function start()
    {
        $this->parent_pid = getmypid();

        $TAGS = InstagramConfig::$TAGS;
        $INDEXES = InstagramConfig::$INDEXES;

        for ($i = 0; $i < count($TAGS); $i++) {
            $dir = dirname(__FILE__) . InstagramConfig::POST_FOLDER . "/" . $INDEXES[$i] . "-" . $TAGS[$i];
            $this->log("get post folder: " . $dir);
            $this->executeThread($dir);
        }
        while (true) { // main thread sit idle, thread will die if parent thread die
            sleep(1);
        }
    }

    private function executeThread($dir)
    {
        $t = new Thread([$this, "startThread"]);
        $t->start($dir);
    }

    public function startThread($dir)
    {
        $this->dir = $dir;
        $part = pathinfo($dir);
        $full_part = explode("-", $part["basename"]);
        $this->index = $full_part[0];
        $this->tag = $full_part[1];

        var_dump($this);

        $lastCheck = 0;
        while (true) {
            // Get a list of queue files
            $queueFiles = glob($this->dir . '/*.queue');
            $lastCheck = time();

            if (count($queueFiles) > 0) {
                $this->log('Found ' . count($queueFiles) . ' queue files to process...');
                // Iterate over each file (if any)
                foreach ($queueFiles as $queueFile) {
                    $this->processQueueFile($queueFile);
                }
            }

            if (!$this->processExists($this->parent_pid)) {
                echo("parent die $this->tag\n");
                break;
            }

            // Wait until ready for next check
            while (time() - $lastCheck < $this->checkInterval) {
                sleep(1);
            }
        }
    }

//    public function process()
//    {
//        $lastCheck = 0;
//
//        while (true) {
//            // Get a list of queue files
//            $queueFiles = glob(InstagramConfig::POST_FOLDER . '/*.queue');
//            $lastCheck = time();
//
//            if (count($queueFiles) > 0) {
//                $this->log('Found ' . count($queueFiles) . ' queue files to process...');
//                // Iterate over each file (if any)
//                foreach ($queueFiles as $queueFile) {
//                    $this->processQueueFile($queueFile);
//                }
//            }
//            // Wait until ready for next check
//            while (time() - $lastCheck < $this->checkInterval) {
//                sleep(1);
//            }
//        }
//    }

    private function processQueueFile($queueFile)
    {
        $string_content = file_get_contents($queueFile);
        $data_content = json_decode($string_content, true);

        $this->log('Processing file: ' . $queueFile . " , data count: " . count($data_content));
        if (isset($data_content)) {
            foreach ($data_content as $data) {
                $mapped = InstagramParser::parseInstagramPost($data);
                $url = InstagramParser::getElasticSearchUrl($this->index, $mapped["id"]);
                $log = CURLService::PUTRequest($url, $mapped);
                $this->log('ES RESPONSE : ' . json_encode($log));
            }
        }

        // move file to backup folder
        $this->createFolder(dirname(dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileService::getRelativePath(dirname(__FILE__), ltrim($queueFile, "."))));
        rename($queueFile, dirname(__FILE__) . InstagramConfig::BACKUP_FOLDER . FileService::getRelativePath(dirname(__FILE__), ltrim($queueFile, ".")));
    }

}

$a = new InstagramPostConsumer();
$a->start();