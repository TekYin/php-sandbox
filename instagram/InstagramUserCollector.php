<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/15/16
 * Time: 11:47 AM
 */

include_once "InstagramConfig.php";
include_once "ServiceBase.php";
include_once "CURLService.php";
include_once "FileService.php";

include_once "Thread.php";

class InstagramUserCollector extends ServiceBase
{

    private $interval;
    private $thread = [];
    private $mutex;

    public function __construct()
    {
        $this->log_file = InstagramConfig::LOG_USER_COLLECTOR;
    }

    

    public function startBatch()
    {
        if (!file_exists('./Users/')) {
            mkdir('./Users/', 0777, true);
        }
        date_default_timezone_set("Asia/Jakarta");
        $this->interval = InstagramConfig::$INTERVAL;
        $start = microtime(true);


        while (true) {
            $current_queue = glob("./UserQueue/Instagram*.queue");
            $this->log('Found ' . count($current_queue) . ' queue files to process...');

            foreach ($current_queue as $file) {
                $this->process_file($file);
            }
            echo("interval: ".$this->interval);
            sleep($this->interval);
        }
        $time_elapsed_secs = microtime(true) - $start;
        $this->log("time elapsed: " . $time_elapsed_secs);
    }

    public function startThread($users)
    {
        $this->log("start thread");
        while (!empty($users) || !empty($this->thread)) {

            while (!empty($users) && count($this->thread) < 5) {
                $worker = new Thread("InstagramUserCollector::threadHandler");
                $worker->start(array_pop($users), $this->mutex);
                $this->thread[] = $worker;
                $this->log("create worker, job left: " . count($users));
            }
//            Mutex::lock($this->mutex);

//            while (!empty($this->thread)) {
                foreach ($this->thread as $index => $thread) {
                    if (!$thread->isAlive()) {
                        $this->log("worker finished");
                        unset($this->thread[$index]);
                    }
                }
//            }

            sleep(1);
        }
        $this->log("end of thread");
    }

    public static function threadHandler($user, $mutex)
    {
        $part = explode("-", $user);
        $user_id = $part[1];
        @error_log($user_id, 0);
        $url = InstagramUserCollector::generateInstagramUserUrl($user_id, InstagramConfig::$ACCESS_TOKEN);
        $userData = CURLService::GETRequest($url)["data"];
        FileService::Write_to_file('./Users/' . "Instagram.UserPost." . $part[0] . ".queue", json_encode($userData));
//        Mutex::unlock($mutex);
    }

    private function process_file($file)
    {
        $content = file_get_contents($file);
        $users = json_decode($content, true);
        $this->log('Found ' . count($users) . ' user files to process...');

        $this->startThread($users);

//        foreach ($users as $user) { // loop all the user to get the User information
//            $part = explode("-", $user);
//            $user_id = $part[1];
//            $this->log($user_id);
//            $url = $this->generateInstagramUserUrl($user_id, InstagramConfig::$ACCESS_TOKEN);
//            $userData = CURLService::GETRequest($url)["data"];
//            FileService::Write_to_file('./Users/' . "Instagram.UserPost." . $part[0] . ".queue", json_encode($userData));
//        }

//        unlink($file);
    }

    public static function generateInstagramUserUrl($user_id, $token)
    {
        $BASE_URL = "https://api.instagram.com/v1/users/%s/?access_token=%s";
        $url = sprintf($BASE_URL, $user_id, $token);
        return $url;
    }

}

$instagram = new InstagramUserCollector();
$instagram->startBatch();