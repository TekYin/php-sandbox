<?php
/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/16/16
 * Time: 2:51 PM
 */
include_once "./CURLService.php";
include_once "./plugins/Detik.php";

class Crawler
{
    public $plugins = [];

    public function start($url)
    {
        // register plugin based on host
        $this->plugins["news.detik.com"] = "Detik";




        $host = parse_url($url)["host"];
        if (array_key_exists($host, $this->plugins)) {
            $class = $this->plugins[$host];
            $result = call_user_func(array($class, 'getMetric'), $url);
            var_dump($result);
        } else {
            echo("No plugin can handle this host: " . $host . "\n");
        }
    }
}

$c = new Crawler();
$c->start("http://news.detik.com/berita/3346807/ahok-jadi-tersangka-pimpinan-ormas-islam-ucapkan-terima-kasih-ke-jokowi");
$c->start("http://news.detik.com/berita/3346782/pengacara-ahok-tak-ajukan-praperadilan");
$c->start("http://regional.kompas.com/read/2016/11/15/18551981/dikritik.soal.banjir.ridwan.kamil.bilang.akan.jawab.dengan.bekerja.");
