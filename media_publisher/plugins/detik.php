<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/16/16
 * Time: 4:00 PM
 */
include_once "PluginBase.php";

//include_once "../CURLService.php";

class Detik extends PluginBase
{
    const emotionUrl = "https://mood.detik.com/api/init?idkanal=&idnews=%s";
    const commentUrl = "https://comment.detik.com/v2/?get&key=%s&group=10";
    private $emotionUrl;
    private $commentUrl;

    public function __construct()
    {
    }

    public static function getEmotion($pId)
    {
        $json = CURLService::GETRequest(sprintf(Detik::emotionUrl, $pId));
        return $json["result"];
    }

    public static function getCommentNum($pId)
    {
        $json = CURLService::GETRequest(sprintf(Detik::commentUrl, $pId));
        return $json["counter"];
    }

    public static function getMetric($url)
    {
        echo("getting metric for url: " . $url . "\n");
//        var_dump(parse_url($url));
        $paths = explode("/", parse_url($url)["path"]);
//        echo("get metric for id: " . $paths[2] . "\n");
        $metric = [];
        $metric["emotion"] = Detik::getEmotion($paths[2]);
        $metric["comment_num"] = Detik::getCommentNum($paths[2]);
        return $metric;
    }

}