<?php

/**
 * Created by PhpStorm.
 * User: TekYin
 * Date: 11/15/16
 * Time: 5:53 PM
 */

include "Instagram/Thread.php";

class Threading
{
    public $i = 2;

    public function start()
    {
        $this->i = 10;
        for ($i = 0; $i < 10; $i++) {
            $t = new Thread([$this, "startThread"]);
            $t->start();
        }
        $this->i = 50;

    }

    public function startThread()
    {
        @error_log("Start thread", 0);
        $this->i = $this->i + rand(5, 10);
        sleep(rand(1, 3));
        @error_log("Thread End: " . $this->i, 0);
    }

}

$thread = new Threading();
$thread->start();